import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import BMICalPage from './pages/BMICalPage';
import LuckyNumPage from './pages/LuckyNumPage';
import Header from './components/Header';

import { Routes,Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
            <Route path="about" element={
                    <AboutUsPage />
                    } />
          
            <Route path="/" element={
                    <BMICalPage />
                   } />

            <Route path="lucky" element={
                    <LuckyNumPage />
                    } />      
        </Routes>
    </div>
  );
}

export default App;