
function AboutUs (props) {  
//กรณีต้องการใช้ props
//<AboutUs  name="xxxxx" /> เขียนใส่ใน App
//<h2>จัดทำโดย: {props.name}</h2>  ใส่ใน return ด้านล้างนี้
     return (
        <div>
            <h2>จัดทำโดย: {props.name}</h2>
            <h2>สามารถติดต่อได้ที่: {props.atwork}</h2>
            <h2>บ้านอยู่ที่: {props.address}</h2>
        </div>
    );
}

export default AboutUs;