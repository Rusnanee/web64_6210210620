import AboutUs from "../components/AboutUs";
function AboutUsPage(){

    return(
        <div>
            <div align ="center">
                <h2>คณะผู้จัดทำ เว็บนี้</h2>

                <AboutUs name="Rusnanee Yusoh"
                         address = "LA" 
                         faculty ="Sci"/>
                <hr />
                <AboutUs name="Mark Tuan"
                         address = "LA" 
                         faculty ="Sci"/>
            </div>
        </div>
    );


}
export default AboutUsPage;