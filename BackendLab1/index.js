const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Wellcome To Backend Lab 1')
})

app.post('/bmi', (req, res) => {

    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if ( !isNaN(weight) && !isNaN(height) ){
        let bmi = weight / (height * height)
        result = {
            "status" : "200",
            "bmi" : bmi
        }
    }
    else{
        result ={
            "status" : "400",
            "massage" : "Weight or Height is not a number"
        }
    }

    res.send(JSON.stringify(result))
  })

  app.get('/triangle', (req, res) => {

    let base = parseFloat(req.query.base)
    let height = parseFloat(req.query.height)
    var result = {}

    if ( !isNaN(base) && !isNaN(height) ){
        let area = (height * base) / 2
        result = {
            "status" : "200",
            "area" : area
        }
    }
    else{
        result ={
            "status" : "400",
            "massage" : "Base or Height is not a number"
        }
    }

    res.send(JSON.stringify(result))
  })

  app.post('/score', (req, res) => {
  
    let score = parseFloat(req.query.score)
    var grade ={}
  
      if( score >= 80 && score <= 100 ) {
      grade = "A"
    } else if( score >= 75 ) {
      grade = "B+"
    } else if( score >= 70 ) {
      grade = "B"
    } else if( score >= 65 ) {
      grade = "C+"
    } else if( score >= 60 ) {
      grade = "C"
    } else if( score >= 55 ) {
      grade = "D+"
    } else if( score >= 50 ) {
      grade = "D"
    } else if( score < 40 && score >= 0  ){
      grade = "E"
    }
  
  res.send(JSON.stringify(req.query.name + "   ===>> GRADE : " + grade))
  })
app.get('/hello', (req, res) => {
    res.send('Sawasdee '+ req.query.name)
  })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})