import logo from './logo.svg';
import './App.css';
import Header from './component/Header';
import Footer from './component/Footer';
import Body from './component/Boby';
import Bio from './component/Bio';

function App() {
  return (
    <div className="App">
      <Header />
        <Body />
        <Bio />
      <Footer />
    </div>
  );
}

export default App;
