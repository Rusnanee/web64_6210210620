import './Header.css'

function Header() {
    return(
        <div id="HeaderBG">
            <h1>นี้คือส่วนหัวของเว็บจ้า</h1>
            <hr />
        </div>
    );
}

export default Header;