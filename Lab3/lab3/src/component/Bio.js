import './Bio.css'

function Bio() {
    return(
        <div id="Bio">
            <h3>ประวิตส่วนตัว</h3>
            ชื่อ นางสาวรุสนานี ยูโส๊ะ <br />
            รหัสนักศึกษา 6210210620 <br />
            ภาควิชา วิทยาการคอมพิวเตอร์ <br />
            สาขา วิทยาศาสตร์การคำนวณ <br />
            คณะ วิทยาศาสตร์
            <hr />
        </div>
    );
}

export default Bio;