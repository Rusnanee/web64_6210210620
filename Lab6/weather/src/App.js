import logo from './logo.svg';
import './App.css';

import { Box, AppBar, Toolbar, Typography, Card} from '@mui/material' ;
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';

import { useEffect, useState } from 'react';
import axios from 'axios';
import ReactWeather, { useOpenWeather } from 'react-open-weather';
import YouTube from 'react-youtube';

function App() {

  const [temperature, setTemperature ] = useState();

  const weatherAPIBeseUrl = 
    "https://api.openweathermap.org/data/2.5/weather?";
  const city = "Songkhla";
  const apikey = "5382d946660e5b79a0adfe755b3ab21c";



  const { data, isLoading, errorMessage } = useOpenWeather({
    key: '5382d946660e5b79a0adfe755b3ab21c',
    lat: '6.8333',
    lon: '100.6667',
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });


  // API KEY 5382d946660e5b79a0adfe755b3ab21c

  useEffect( () => {
    setTemperature("-----");

    axios.get(weatherAPIBeseUrl+"q="+city+"&appid="+apikey).then ( (response) => {
      let data = response.data;
      let temp = Math.fround(data.main.temp-273) ;
      setTemperature(temp.toFixed(2));
    })
  }
, [] );


  return (
    <Box sx={{ flexGrow : 1, width : "100%" }}>
      <AppBar position='static'>
        <Toolbar>
          <Typography variant="h6">
            WeatherApp
          </Typography>
        </Toolbar>
      </AppBar>
      
      <Box sx={{ justifyContent : 'center', marginTop : "20px", width : "100%", display:"flex" }}>
        <Typography variant="h4">
          อากาศหาดใหญ่วันนี้
        </Typography>
      </Box>

      <Box sx={{ justifyContent : 'center', marginTop : "20px", width : "100%", display:"flex" }}>
        <Card sx={{ width : 275 }}>
            <CardContent>
              <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                อุณหภูมิหาดใหญ่วันนี้คือ
              </Typography>
              <Typography variant="h5" component="div">
                {temperature}
              </Typography>
              <Typography variant="h6" component="div">
                องศาเซลเซียส
              </Typography>
            </CardContent>
        </Card>

        <ReactWeather
      isLoading={isLoading}
      errorMessage={errorMessage}
      data={data}
      lang="en"
      locationLabel="Munich"
      unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
      showForecast
    />

      </Box>
      <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
      <h3>ขอบคุณนะคะ อาจารย์ชินนา</h3>
      <YouTube videoId='-yMPqLQWJlQ'/>
      <YouTube videoId='b3DQYIj1Ib4'/>
      <YouTube videoId='n-NHWOqPXWc'/>
    </Box>
  );
}

export default App;
